#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 14:42:22 2022

@author: kaouther
"""
import abc

class Edege:
    
    def __init__(self,index,bw,a_t_b):
        self.index=index            
        self.bandwidth = bw
        self.nodeindex=[int(a_t_b[0]),int(a_t_b[1])]

    @abc.abstractmethod   
    def __str__(self):
        ''' This method returns a string of node characteristics to be printed in msg'''
        return 
    
    def msg(self):
        print(self.__str__())


class Vedege(Edege):
    
    def __init__(self,index,bw,a_t_b):
        super().__init__(index, bw, a_t_b)
        self.spc = []    
        
    def __str__(self):
        return 'vedege',self.index,'bandwidth',self.bandwidth,'nodeindex',self.nodeindex,'spc',self.spc
    

class Sedege(Edege):
    
    def __init__(self,index,bw,a_t_b):
        super().__init__(index, bw, a_t_b)
        self.lastbandwidth = bw
        self.vedegeindexs = []
        self.open=False
        self.mappable_flag = False    
        
    def __str__(self):
        return 'sedege', self.index, 'bandwidth', self.bandwidth,'lastbandwidth',self.lastbandwidth, 'nodeindex', self.nodeindex,'vedegeindexs',self.vedegeindexs