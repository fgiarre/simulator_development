#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 14:43:05 2022

@author: kaouther
"""
from node import Vnf
from edege import Vedege
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng
import torch
import json
import dgl
import dgl.function as fn



class VNR:
    ID_ = 0 
    
    def __init__(self,env,vnf_range, cpu_range, bw_range,flavor_size,vnr_class,duration,mtbs):
        VNR.ID_+=1
        with open('parameters.json', 'r') as openfile: 
            self.json_object = json.load(openfile) 
        self.vnr_class=vnr_class   # vnr class
        self.mtbs=mtbs             # mean time between scale demande            
        self.env=env
        self.id = VNR.ID_
        self.num_vnfs =vnf_range[0] #np.random.randint(self.vnf_range[0], self.vnf_range[1])
        self.p = (3 * np.log(self.num_vnfs)) / self.num_vnfs
        self.graph = nx.erdos_renyi_graph(self.num_vnfs, self.p, directed=False)
        self.num_vedges = len(self.graph.edges())
        self.edges = list(self.graph.edges())
        self.time = self.env.now
        self.duration  = duration
        self.vnode = []
        self.vedege = []
        self.nodemapping = []
        self.edgemapping = []
        p_scalingUp=int(1/2*duration/mtbs)+1
        for i in range(self.num_vnfs):
            cpu=np.random.randint(cpu_range[0],cpu_range[1]/2)
            vno = Vnf(i,cpu,cpu_range[1],self.id,flavor_size,p_scalingUp)
            self.vnode.append(vno)
        for i in range(self.num_vedges):
            a_t_b = list(self.graph.edges())[i]
            bw = np.random.randint(bw_range[0],bw_range[1])
            ved = Vedege(i,bw,a_t_b)
            self.vedege.append(ved)
        for n in self.vnode:
            for e in self.vedege:
                if n.index in e.nodeindex:
                    n.bw += e.bandwidth
        for el in self.vnode:
            el.neighbors = [n for n in self.graph.neighbors(el.index)]
        for el in self.vnode:
            el.degree = len(el.neighbors)    
        
       
    # For improved interfacing with OpenAI Env -- to be improved
    def now(self):
        return self.env.now
    
    def action_sample(self):
        return np.random.uniform(0,1,self.json_object['numnodes'])
    
    def getG(self):
        bandwidth = dict()
        for i in range (len(self.graph.edges())):
            ed = self.vedege[i]
            bandwidth[(ed.nodeindex[0],ed.nodeindex[1])] = ed.bandwidth
        nx.set_edge_attributes(self.graph, bandwidth,'bandwidth')
        return self.graph
    
    def drawVNR(self):
        plt.figure()
        g = self.getG()
        pos = nx.fruchterman_reingold_layout(g)
        nx.draw(g, node_color=[[0.5, 0.8, 0.5]], font_size=8, node_size=300, with_labels=True, nodelist=g.nodes())
        nx.draw_networkx_edge_labels(g, pos,edge_labels={edege: g[edege[0]][edege[1]]["bandwidth"] for edege in g.edges()})
        plt.show()
        
    def msg(self):
        print("vnr_id",self.id,"duration",self.duration)
        for i in range(len(self.vnode)):
            self.vnode[i].msg()
        for i in range(len(self.vedege)):
            self.vedege[i].msg()
    
    def generate_scale_up(self):
        rng = default_rng()
        random_chain= np.sort(rng.choice(self.num_vnfs, size=np.random.randint(1,self.num_vnfs+1), replace=False))
        scaling_chaine=[]
        for i in range(len(random_chain)):
            j=random_chain[i]
            if self.vnode[j].cpu_index< len(self.vnode[j].flavor)-1:
                scaling_chaine.append(j)
                self.vnode[j].cpu_index+=1
                self.vnode[j].req_cpu=self.vnode[j].flavor[self.vnode[j].cpu_index]-self.vnode[j].cpu
        return scaling_chaine  
            
    def generate_scale_down(self):
        rng = default_rng()
        random_chain= np.sort(rng.choice(self.num_vnfs, size=np.random.randint(1,self.num_vnfs+1), replace=False))
        scaling_chaine=[]
        for i in range(len(random_chain)):
            j=random_chain[i]
            if self.vnode[j].cpu_index> 0:
                scaling_chaine.append(j)
                self.vnode[j].cpu_index-=1
                self.vnode[j].req_cpu=self.vnode[j].cpu-self.vnode[j].flavor[self.vnode[j].cpu_index]
        return scaling_chaine
        

            
    def get_features(self,index):
        '''a modifier selon features qu'on va entrer '''
        id_neighbors=self.vnode[index].neighbors
        id_all=[index]+id_neighbors
        #concatenate indexes, the node index first then the neighbors
        cpu = torch.tensor([el.cpu for el in self.vnode]).unsqueeze(1)
        cpu =np.true_divide(cpu,cpu.max(0, keepdim=True)[0])
        bw = torch.tensor([el.bw for el in self.vnode]).unsqueeze(1)
        bw = np.true_divide(bw,bw.max(0, keepdim=True)[0])
        current = torch.tensor([el.current for el in self.vnode]).unsqueeze(1)
        visited = torch.tensor([el.visited for el in self.vnode]).unsqueeze(1)
        degree = torch.tensor([el.degree for el in self.vnode]).unsqueeze(1)
        flavor= torch.tensor([el.flavor[len(el.flavor)-1] for el in self.vnode]).unsqueeze(1)
        flavor =np.true_divide(flavor,flavor.max(0, keepdim=True)[0])
        #indicate which vnf to place
        one_hot=[0]*self.num_vnfs
        one_hot[index]=1
        one_hot= torch.tensor(one_hot).unsqueeze(1)
        one_hot= np.true_divide(one_hot,one_hot.max(0, keepdim=True)[0])
        degree=np.true_divide(degree,degree.max(0, keepdim=True)[0])
        remainingnodes=torch.tensor([(self.num_vnfs-el)/self.num_vnfs for el in range(self.num_vnfs)]).unsqueeze(1)
        remainingedges=torch.tensor([self.num_vedges]*self.num_vnfs).unsqueeze(1)
        feature = torch.cat((cpu,bw,degree,flavor,one_hot),1).float()
        #feature = torch.cat((cpu,bw,degree),1).float()
        feature_ordered=feature[id_all]
        return feature
    
    def get_allfeatures(self):
        cpu = torch.tensor([el.cpu for el in self.vnode]).unsqueeze(1)
        cpu =np.true_divide(cpu,cpu.max(0, keepdim=True)[0])
        bw = torch.tensor([el.bw for el in self.vnode]).unsqueeze(1)
        bw = np.true_divide(bw,bw.max(0, keepdim=True)[0])
        current = torch.tensor([el.current for el in self.vnode]).unsqueeze(1)
        visited = torch.tensor([el.visited for el in self.vnode]).unsqueeze(1)
        degree = torch.tensor([el.degree for el in self.vnode]).unsqueeze(1)
        #degree=np.true_divide(degree,degree.max(0, keepdim=True)[0])
        remainingnodes=torch.tensor([(self.num_vnfs-el)/self.num_vnfs for el in range(self.num_vnfs)]).unsqueeze(1)
        remainingedges=torch.tensor([self.num_vedges]*self.num_vnfs).unsqueeze(1)
        feature = torch.cat((cpu,bw,degree),1).float()
        #torch.tensor(feature).unsqueeze(0)
        return feature
    
     
    def p_loadRset(self,sb):
        for j in range(self.num_vnfs):
            i=self.nodemapping[j]
            if (i>-1):
                sb.snode[i].p_load=(sb.snode[i].p_load*sb.snode[i].cpu-self.vnode[j].p_maxCpu)/sb.snode[i].cpu
                if sb.snode[i].p_load<0:
                    sb.snode[i].p_load=0.0
            
    
    def EndsVnr(self,sb,VNRSS):
        #update substrate resources after vnr freeing the allocated resources
        #VNRindex=[VNRSS.reqs_ids[index] for index in range(VNRSS.num_reqs)]
        VNRindex=VNRSS.reqs_ids.index(self.id)
        self.p_loadRset(sb)
        sb.removenodemapping(VNRSS.reqs[VNRindex])
        sb.removeedegemapping(VNRSS.reqs[VNRindex])
        # must also remove VNR from VNRSS
        VNRSS.reqs_ids.remove(VNRSS.reqs_ids[VNRindex])
        VNRSS.reqs.remove(VNRSS.reqs[VNRindex])
        VNRSS.vedges.remove(VNRSS.vedges[VNRindex])
        VNRSS.vnfs.remove(VNRSS.vnfs[VNRindex])
        VNRSS.num_reqs-=1
        #print(f'VNR {self.id} is leaving')
        del self
        return
    
    def DropVnr(self,VNRSS):
        #update substrate resources after vnr freeing the allocated resources
        #VNRindex=[VNRSS.reqs_ids[index] for index in range(VNRSS.num_reqs)]
        VNRindex=VNRSS.reqs_ids.index(self.id)
        # must also remove VNR from VNRSS
        VNRSS.reqs_ids.remove(VNRSS.reqs_ids[VNRindex])
        VNRSS.reqs.remove(VNRSS.reqs[VNRindex])
        VNRSS.vedges.remove(VNRSS.vedges[VNRindex])
        VNRSS.vnfs.remove(VNRSS.vnfs[VNRindex])
        VNRSS.num_reqs-=1
        del self
        return
    
    def getNetworkx(self):
        return self.graph 
    
    def getFeatures(self):
        cpu = torch.from_numpy(np.squeeze([el.cpu for el in self.vnode]))
        cpu = torch.unsqueeze(cpu, dim=0).numpy()
        bw = torch.from_numpy(np.squeeze([el.bw for el in self.vnode]))
        bw = torch.unsqueeze(bw, dim=0).numpy()
        p_maxCpu = torch.from_numpy(np.squeeze([el.p_maxCpu for el in self.vnode]))
        p_maxCpu = torch.unsqueeze(p_maxCpu, dim=0).numpy()
        features = np.transpose(np.concatenate((cpu, bw, p_maxCpu)))
        return features
    
    def __del__(self):
        return
    

