#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:01:26 2022

@author: kaouther
"""

import torch
import numpy as np


class ReplayBuffer(object):
    def __init__(self,max_size): #,input_shape,n_actions):
        self.mem_size = max_size
        self.mem_cntr = 0
        self.state_memory = [0]*max_size
        self.new_state_memory = [0]*max_size
        self.action_memory = np.zeros(self.mem_size ,dtype= np.int64)
        self.reward_memory = np.zeros(self.mem_size, dtype = np.float64)
        self.terminal_memory = np.zeros(self.mem_size, dtype = np.uint8)
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    def store(self, state, action, reward, state_, done):
        index =  self.mem_cntr % self.mem_size
        self.state_memory[index] = state
        self.new_state_memory[index] = state_
        self.action_memory[index] = action
        self.reward_memory[index] = reward
        self.terminal_memory[index] = done
        self.mem_cntr += 1
    def sample_buffer(self,batch_size):
        max_mem = min(self.mem_cntr, self.mem_size)
        batch = np.random.choice(max_mem,batch_size,replace= False)
        actions = self.action_memory[batch]
        rewards = self.reward_memory[batch]
        dones = self.terminal_memory[batch]
        states = []
        states_ = []
        for el in batch:
            states.append(self.state_memory[el])
            states_.append(self.new_state_memory[el])

        return states, actions, rewards, states_, dones
