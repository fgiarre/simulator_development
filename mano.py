#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 14:43:55 2022

@author: kaouther
"""

import numpy as np
import sys
class ManoSimulator():
    
    def __init__(self,solver,sn,env):
        
        self.obtained_results=[]
        self.episodes_batch=20
        self.episode_metric=0
        self.episode_r2c=0
        self.episode_pLoad=0
        self.episodes_reject=0
        self.episode_nb_scale=0
        self.episode_nb_remap=0
        self.episode_charge=0
        self.episode_nb_used_nodes=0
        self.episode_pCharge=0
        self.episodes_cpt=0
        self.episodes_results=[]
        self.solver=solver
        self.env=env
        self.subNet=sn
        
    def integration_test(self,VNRSS):
        #check if placement/remmaping in nodes is done correctly 
        for snode in self.subNet.snode:
            cpu_used=0
            p_load=0
            vnodeindexs=snode.vnodeindexs
            for vnode in vnodeindexs:
                vnr_id=vnode[0]
                VNRindex=VNRSS.reqs_ids.index(vnr_id)
                vnr=VNRSS.reqs[VNRindex]
                if vnr.vnode[vnode[1]].sn_host == snode.index:
                   cpu_used+=vnr.vnode[vnode[1]].cpu
                   p_load=(vnr.vnode[vnode[1]].p_maxCpu+p_load*snode.cpu)/snode.cpu
                else:
                    print(vnr.vnode[vnode[1]].sn_host,snode.index)
                    sys.exit("bad node palcement")
            if snode.lastcpu+cpu_used != snode.cpu:
                print(cpu_used,snode.index)
                sys.exit("error in lastcpu calculation")
            if abs(snode.p_load - p_load )>0.0001 :
                print(snode.index)
                print(p_load)
                print(snode.p_load)
                self.subNet.msg()
                sys.exit("error in pload calculation")
        #check if placement/remmaping in edeges is done correctly 
        for sedege in self.subNet.sedege:
            used_bw=0
            vedegeindexs=sedege.vedegeindexs
            for vedege in vedegeindexs:
                vnr_id=vedege[0]
                VNRindex=VNRSS.reqs_ids.index(vnr_id)
                vnr=VNRSS.reqs[VNRindex]
                if sedege.index  in vnr.vedege[vedege[1]].spc:
                    used_bw+=vnr.vedege[vedege[1]].bandwidth
                else: 
                    sys.exit("bad edege palcement")
            if sedege.lastbandwidth+used_bw!= sedege.bandwidth:
                    sys.exit("error in bw calculation")
        #check lastbw in nodes:
        for n in self.subNet.snode:
            lastbw = 0
            for e in self.subNet.sedege:
                if n.index in e.nodeindex:
                    lastbw += e.lastbandwidth
            if n.lastbw != lastbw:
                sys.exit("error in lastbw calculation in node")
                
    def network_load(self):
        network_load=0
        nb_used=0
        for snode in self.subNet.snode:
            if len(snode.vnodeindexs)>0:
                nb_used+=1
                network_load+=(snode.cpu-snode.lastcpu)/snode.cpu
        return network_load/self.subNet.num_nodes, nb_used

    def vnr_life_cycle(self,VNRSS,vnr):
        VNR=VNRSS.reqs[VNRSS.num_reqs-1]
        #VNE if done, drop request if not
        time=self.env.now
        #print(f'mapping: time is {time}')
        success,v2sindex,ve2seindex,r2c,p_load,metric,self.subNet=self.solver.mapping(self.subNet, vnr)
        if not success:
            self.episodes_reject+=1
        charge,nb_used=self.network_load()
        self.episode_nb_used_nodes+=nb_used
        self.episode_charge+=charge
        self.episodes_cpt+=1
        self.episode_metric+=metric
        self.episode_r2c+=r2c
        self.episode_pLoad+=p_load
        if self.episodes_cpt % self.episodes_batch ==0:
            self.episodes_results.append([self.episode_metric/self.episodes_batch,self.episode_r2c/self.episodes_batch,self.episode_pLoad/self.episodes_batch,self.episodes_reject/self.episodes_batch,self.episode_nb_remap/self.episode_nb_scale,self.episode_charge/self.episodes_batch,self.episode_nb_used_nodes/self.episodes_batch])
            self.episode_nb_used_nodes=0
            self.episode_charge=0
            self.nb_used_nodes=0
            self.episode_metric=0
            self.episode_r2c=0
            self.episode_pLoad=0
            self.episodes_reject=0
            self.episode_nb_scale=0
            self.episode_nb_remap=0
        self.integration_test(VNRSS)
        VNRSS.reqs[VNRSS.num_reqs-1]=VNR
        sum_vnfs=0   
        for i,el in enumerate(VNRSS.vnfs):
            sum_vnfs+=len(el[1])
        #print(f'nb of deployed VNFs: {sum_vnfs}')
        sum_vedges=0
        for i,el in enumerate(VNRSS.vedges):
            sum_vedges+=len(el[1])
        #print(f'nb of deployed Vedges: {sum_vedges}')
        if success==True:
            vnr_deployed=VNRSS.num_reqs
            self.obtained_results.append([self.env.now,metric,VNRSS.num_reqs,sum_vnfs,sum_vedges,vnr_deployed])
            ###################################
            #generate scale demands
            ###################################
            end=self.env.now+vnr.duration
            while self.env.now<end:
                next_scale= np.random.exponential(vnr.mtbs)
                if self.env.now+next_scale <end:
                    scaling_chaine=[]
                    scale_up=False
                    u=np.random.uniform(0,1)
                    if u<1/2:
                        #scale up
                        scale_up=True
                        scaling_chaine=vnr.generate_scale_up()
                    else:
                        scaling_chaine=vnr.generate_scale_down()
                    if len(scaling_chaine)>0:
                        yield self.env.timeout(next_scale)
                        self.episode_nb_scale+=1
                        if scale_up:
                            success,v2sindex,ve2seindex,r2c,p_load,metric,remap,self.subNet=self.solver.scaling_up(vnr, self.subNet, scaling_chaine)
                            if remap:
                                self.episode_nb_remap+=1
                                if not success:
                                    self.episodes_reject+=1
                                charge,nb_used=self.network_load()
                                self.episode_nb_used_nodes+=nb_used
                                self.episode_charge+=charge
                                self.episodes_cpt+=1
                                self.episode_metric+=metric
                                self.episode_r2c+=r2c
                                self.episode_pLoad+=p_load
                                if self.episodes_cpt % self.episodes_batch ==0:
                                    self.episodes_results.append([self.episode_metric/self.episodes_batch,self.episode_r2c/self.episodes_batch,self.episode_pLoad/self.episodes_batch,self.episodes_reject/self.episodes_batch,self.episode_nb_remap/self.episode_nb_scale,self.episode_charge/self.episodes_batch,self.episode_nb_used_nodes/self.episodes_batch])
                                    self.episode_nb_used_nodes=0
                                    self.episode_charge=0
                                    self.nb_used_nodes=0
                                    self.episode_metric=0
                                    self.episode_r2c=0
                                    self.episode_pLoad=0
                                    self.episodes_reject=0
                                    self.episode_nb_scale=0
                                    self.episode_nb_remap=0
                            if not success:
                                break
                        else:
                            self.subNet=self.solver.scaling_down(vnr,self.subNet,scaling_chaine)
                        self.integration_test(VNRSS)
                else:
                    #no scaling
                    yield self.env.timeout(end-self.env.now)
            ###################################
            vnr.EndsVnr(self.subNet,VNRSS)
            self.integration_test(VNRSS)
            ###################################
        else:
            metric=-1
            vnr_deployed=VNRSS.num_reqs-1
            self.obtained_results.append([self.env.now,metric,VNRSS.num_reqs,sum_vnfs,sum_vedges,vnr_deployed])
            #print(f'VNR {VNR.ID_} is not well deployed: node: {success}, edges: {lsuccess}')
            vnr.DropVnr(VNRSS)
            self.integration_test(VNRSS)
        return 
    