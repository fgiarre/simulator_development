#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 16:02:36 2022

@author: kaouther
"""
import numpy as np
from vnr import VNR

class VNRS :
    
    def __init__(self,num_reqs):
        self.num_reqs = num_reqs
        self.reqs_ids=[]
        self.reqs = []
        self.vnfs = []
        self.vedges = []
        
    def msg(self):
        n = self.num_reqs
        for i in range(n):
            print('----------------------')
            self.reqs[i].msg()
    

class Generator():
    
    def __init__(self,vnr_classes,mlt,mtbs,mtba,vnfs_range,vcpu_range,vbw_range,flavor_tab,p_flavors):
        self.vnr_classes=vnr_classes
        self.mlt=mlt
        self.mtbs=mtbs
        self.mtba=mtba
        self.vnfs_range=vnfs_range
        self.vcpu_range=vcpu_range
        self.vbw_range=vbw_range
        self.flavor_tab=flavor_tab
        self.p_flavors=p_flavors
        
    def generate_flavor(self):
           u=np.random.uniform(0,1)
           p_sum=0
           for i in range (len(self.flavor_tab)):
               p_sum+=self.p_flavors[i]
               if u<p_sum:
                   return self.flavor_tab[i]
        
    def vnr_ClassGenrator(self):
        nb_classes=len(self.vnr_classes)
        u=np.random.uniform(0,1)
        p_sum=0
        for i in range(nb_classes):
            p_sum+=self.vnr_classes[i]
            if u<p_sum:
                return i
       
    def VnrGenerator_poisson(self,env,sb,manoSimulator):
        VNRSS=VNRS(0)
        cpt=0
        while True:
            next_arrival = np.random.exponential(self.mtba)
            yield env.timeout(next_arrival)
            vnr_class=self.vnr_ClassGenrator()
            duration  = np.random.exponential(self.mlt[vnr_class])
            flavor_size=self.generate_flavor()
            d=VNR(env,self.vnfs_range,self.vcpu_range,self.vbw_range,flavor_size,vnr_class,duration,self.mtbs[vnr_class])
            VNRSS.reqs.append(d)
            VNRSS.reqs_ids.append(d.id)
            VNRSS.vnfs.append((VNRSS.reqs[VNRSS.num_reqs].id,VNRSS.reqs[VNRSS.num_reqs].vnode))
            VNRSS.vedges.append((VNRSS.reqs[VNRSS.num_reqs].id,VNRSS.reqs[VNRSS.num_reqs].vedege))
            VNRSS.num_reqs+=1
            cpt=cpt+1
            env.process(manoSimulator.vnr_life_cycle(VNRSS,d))
        return