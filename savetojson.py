#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:50:33 2020

@author: ghina
"""
import json

dictionary={
    "OUTPUT_PATH"   : "Results/",
    "SIM_TIME"      : [150000],#[4100,20100,40100,80100,160100],               # Simulation time in seconds
    "start_mean_calculation": 1000,
    "MST"           : 12,                     # Mean Service Time (not considered)
    "REPEAT_EXPERIENCE" : 1,
    "Controller"    : "GNNDRL",
    "MTBA"          : [5],  #[1,5,10,20,40],
    "MLT"           : [7,7,7], # lifetime mean of each class
    "vnr_classes"   : [0.2, 0.3, 0.5],  # vnr classes proportion must be in descending order
    "MTBS"          : [2,3,4], # each class of vnr has a mean time between scale time demande  
    "flavors"       : [5,3,4], # for each class we choose the size of the flavor randomly
    "p_flavors"      : [0.1,0.3,0.6], #eah flavor has a proportion, they must be in descending order of their proportion
    # for example we have flovor 1 with proportion 0.8 and flavor 5 with proportion 0.2 the table of flavors must be [5,1] and p_flavors [0.2,0.8]
    "alpha"         : 1,
    "beta"          : 0.5,
    "cpu_range"     : (50,100),
    "numnodes"      : 24,
    "bw_range"      : (50,100),
    "num_reqs"      : 1,
    "vnfs_range"    : (5,5),
    "vcpu_range"    : (1,20),
    "vbw_range"     : (5,10),
    "start_timesteps" : 30,
    "start_decay"     : 30000,
    "stop_decay"      : 80000,
    "vnr_duration_min"  : 5,
    "vnr_duration_max"  : 10,
    "egreedy"           : False,
    "epsilon"           : 0.7,
    "e_decay"           : 0,
    "gamma"             : 0.95,
    "N_iter"            : 4,
    "expl_noise"        : 0.1,
    "policy_noise"      : 0.5,
    "noise_clip"       : 0.5,
}





# Serializing json  
json_object = json.dumps(dictionary, indent = 5) 
  
# Writing to sample.json 
with open("parameters.json", "w") as outfile: 
    outfile.write(json_object) 
