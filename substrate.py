#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 14:42:55 2022

@author: kaouther
"""
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import torch
import json
from node import Snode
from edege import Sedege

class SN :
    
    def __init__(self, num_nodes, cpu_range, bw_range,topology):
        with open('parameters.json', 'r') as openfile: 
            json_object = json.load(openfile) 
        self.num_nodes = json_object['numnodes']
        self.cpu_range = cpu_range
        self.bw_range = bw_range
        self.snode = []
        self.sedege = []
        # add dgl before self.g
        self.g = topology #nx.waxman_graph(num_nodes,self.beta,self.alpha)
        self.edges = list(self.g.edges())
        self.numedges = len(self.edges)
        
        for i in range(self.num_nodes):
            cpu = np.random.randint(cpu_range[0],cpu_range[1])
            self.snode.append(Snode(i,cpu))
        for i in range(self.numedges):
            bw = np.random.randint(self.bw_range[0], self.bw_range[1])
            a_t_b = [self.edges[i][0], self.edges[i][1]]
            self.sedege.append(Sedege(i, bw, a_t_b))
        for n in self.snode:
            for e in self.sedege:
                if n.index in e.nodeindex:
                    n.bw += e.bandwidth
            n.lastbw = n.bw
        for el in self.snode:
            el.neighbors = [n for n in self.g.neighbors(el.index)]
        for el in self.snode:
            el.degree = len(el.neighbors)
        
    def updateCpu(self,nodemapping,req_cpu):
        for i in range(len(nodemapping)):
            self.snode[nodemapping[i]].updateCpu(req_cpu[i])
            
    def remCpu(self,nodemapping):
            remCpu=[]
            for i in nodemapping:
                remCpu.append(self.snode[i].lastcpu)  
            return remCpu
        
    def removenodemapping(self, vnr):
        sn = len(self.snode)
        vn = len(vnr.vnode)
        for i in range(sn):
            xtemp = []
            for x in self.snode[i].vnodeindexs:
                if vnr.id == x[0]:
                    self.snode[i].lastcpu = self.snode[i].lastcpu + vnr.vnode[x[1]].cpu
                    xtemp.append(x)
                    # self.snode[i].vnodeindexs.remove(x)
            for x in xtemp:
                self.snode[i].vnodeindexs.remove(x)
        for j in range(vn):
            vnr.vnode[j].sn_host = None
            
    def removeedegemapping(self, vnr):
        en = len(self.sedege)
        vn = len(vnr.vedege)
        for e in range(en):
            tempx = []
            for x in self.sedege[e].vedegeindexs:
                if vnr.id == x[0]:
                    self.sedege[e].lastbandwidth = self.sedege[e].lastbandwidth + vnr.vedege[x[1]].bandwidth
                    tempx.append(x)
            for x in tempx:
                self.sedege[e].vedegeindexs.remove(x)
            if not self.sedege[e].vedegeindexs:
                self.sedege[e].open = False
        for n in self.snode:
            n.lastbw = 0
            for e in self.sedege:
                if n.index in e.nodeindex:
                    n.lastbw += e.lastbandwidth
        for ve in range(vn):
            vnr.vedege[ve].spc = []
            
    def get_features(self):
        cpu = torch.tensor([el.cpu for el in self.snode]).unsqueeze(1)
        bw = torch.tensor([el.bw for el in self.snode] ,).unsqueeze(1)
        lastcpu = torch.tensor([el.lastcpu for el in self.snode]).unsqueeze(1)
        lastcpu = np.true_divide(lastcpu,lastcpu.max(0, keepdim=True)[0])
        p_load= torch.tensor([el.p_load for el in self.snode]).unsqueeze(1)
        for n in self.snode:
            n.lastbw = 0
            for e in self.sedege:
                if n.index in e.nodeindex:
                    n.lastbw += e.lastbandwidth
        lastbw= torch.tensor([el.lastbw for el in self.snode]).unsqueeze(1)
        lastbw = np.true_divide(lastbw,lastbw.max(0, keepdim=True)[0])
        degree = torch.tensor([el.degree for el in self.snode]).unsqueeze(1)
        zeros = torch.tensor([0]*len(self.snode)).unsqueeze(1)
        degree = np.true_divide(degree,degree.max(0, keepdim=True)[0])
        feature = torch.cat((lastcpu, lastbw, degree,p_load,zeros), 1).float()
        #feature = torch.cat((lastcpu, lastbw, degree), 1).float()
        return feature
    
    def Sn2_networkxG(self, bandlimit=0):
        g = nx.Graph()
        for snod in self.snode:
            g.add_node(snod.index, index=snod.index, cpu=snod.cpu, lastcpu=snod.lastcpu)
        en = len(self.sedege)
        for i in range(en):
            if self.sedege[i].lastbandwidth > bandlimit:
                g.add_edge(self.sedege[i].nodeindex[0], self.sedege[i].nodeindex[1], index=self.sedege[i].index,lastbandwidth=self.sedege[i].lastbandwidth, bandwidth=self.sedege[i].bandwidth,capacity=1)
        return g
    
    def drawSN(self,edege_label=False,classflag=False):
        plt.figure()
        g = self.Sn2_networkxG()
        pos = nx.fruchterman_reingold_layout(g)
        if classflag:
            color = {}
            colomap = []
            for i in range(len(self.snode)):
                if self.snode[i].classflag not in color.keys():
                    color[self.snode[i].classflag] = self.randRGB()
                colomap.append(color[self.snode[i].classflag])
            nx.draw(g, node_color=colomap, font_size=8, node_size=300, pos=pos, with_labels=True,
                    nodelist=g.nodes())
            if  edege_label:nx.draw_networkx_edge_labels(g, pos, edge_labels={edege: g[edege[0]][edege[1]]["lastbandwidth"] for edege in g.edges()})
        else:
            nx.draw(g, node_color=[[0.5, 0.8, 0.8]], font_size=8, node_size=300, pos=pos, with_labels=True,nodelist=g.nodes())
            if  edege_label:nx.draw_networkx_edge_labels(g, pos, edge_labels={edege: g[edege[0]][edege[1]]["lastbandwidth"] for edege in g.edges()})
        plt.show()
        
    def randRGB(self):
        return (np.random.randint(0, 255) / 255.0,
                np.random.randint(0, 255) / 255.0,
                np.random.randint(0, 255) / 255.0)

    def getNetworkx(self):
        return self.g

    def msg(self):
        n = len(self.snode)
        for i in range(n):
            print('--------------------')
            self.snode[i].msg()
        n = len(self.sedege)
        for i in range(n):
            print('--------------------')
            self.sedege[i].msg()
     
    def getFeatures(self):
        cpu = torch.from_numpy(np.squeeze([el.lastcpu for el in self.snode]))
        cpu = torch.unsqueeze(cpu, dim=0).numpy()
        bw = torch.from_numpy(np.squeeze([el.bw for el in self.snode]))
        bw = torch.unsqueeze(bw, dim=0).numpy()
        p_load = torch.from_numpy(np.squeeze([el.p_load for el in self.snode]))
        p_load = torch.unsqueeze(p_load, dim=0).numpy()
        features = np.transpose(np.concatenate((cpu, bw,p_load)))
        return features