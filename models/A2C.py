#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 10:12:48 2022

@author: kaouther
"""


import torch  
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import dgl
from models.GCN import GCNModule
from models.attention import Sn_AttentionLayer,Vnr_AttentionLayer
from models.neuralTensor import NeuralTensorNetwork
from collections import OrderedDict
import numpy as np
import os

class ActorCriticOneNetwork(nn.Module):
    def __init__(self, num_inputs, num_actions, hidden_size,n_hidden_layers=4):
        super(ActorCriticOneNetwork, self).__init__() 
        self.n_inputs = num_inputs
        self.n_outputs = num_actions
        self.n_hidden_nodes =hidden_size
        self.n_hidden_layers = n_hidden_layers
        self.action_space = np.arange(self.n_outputs)
        
        # Generate network according to hidden layer and node settings
        self.layers = OrderedDict()
        self.n_layers = 2 * self.n_hidden_layers
        for i in range(self.n_layers + 1):
            # Define single linear layer
            if self.n_hidden_layers == 0:
                self.layers[str(i)] = nn.Linear(self.n_inputs,self.n_outputs)
            # Define input layer for multi-layer network
            elif i % 2 == 0 and i == 0 and self.n_hidden_layers != 0:
                self.layers[str(i)] = nn.Linear(self.n_inputs,self.n_hidden_nodes)
            # Define intermediate hidden layers
            elif i % 2 == 0 and i != 0:
                self.layers[str(i)] = nn.Linear(self.n_hidden_nodes,self.n_hidden_nodes)
            else:
                self.layers[str(i)] = nn.ReLU()
                
        self.body = nn.Sequential(self.layers)
            
        # Define policy head
        self.policy = nn.Sequential(
            nn.Linear(self.n_hidden_nodes,self.n_hidden_nodes),
            nn.ReLU(),
            nn.Linear(self.n_hidden_nodes,self.n_outputs))
        # Define value head
        self.value = nn.Sequential(nn.Linear(self.n_hidden_nodes,self.n_hidden_nodes),
            nn.ReLU(),
            nn.Linear(self.n_hidden_nodes,1))
        
    def forward(self,state):
        body_output=self.body(state)
        policy_dist = F.softmax(self.policy(body_output), dim=1)
        value=self.value(body_output)
        return value,policy_dist
        

class ActorCritic(nn.Module):
    def __init__(self, num_inputs, num_actions, hidden_size):
        super(ActorCritic, self).__init__()

        self.num_actions = num_actions
        self.critic_linear1 = nn.Linear(num_inputs, hidden_size)
        self.critic_linear2 = nn.Linear(hidden_size, 1)

        self.actor_linear1 = nn.Linear(num_inputs, hidden_size)
        self.actor_linear2 = nn.Linear(hidden_size, num_actions)
    
    def forward(self, state):
        value = F.relu(self.critic_linear1(state))
        value = self.critic_linear2(value)
        
        policy_dist = F.relu(self.actor_linear1(state))
        policy_dist = F.softmax(self.actor_linear2(policy_dist), dim=1)

        return value, policy_dist


class GNNA2C(nn.Module):
    
    def __init__(self, num_inputs_sn,num_inputs_vnr, hidden_size,GCN_out,tensor_neurons,num_actions,learning_rate,actorCritic):
        super(GNNA2C,self).__init__()
        self.learning_rate=learning_rate
        self.gcn_vnr=GCNModule(num_inputs_vnr, hidden_size, GCN_out, torch.relu)
        self.gcn_sn=GCNModule(num_inputs_sn, hidden_size, GCN_out, torch.relu)
        self.att_vnr=Vnr_AttentionLayer(GCN_out, torch.tanh)
        self.att_sn=Sn_AttentionLayer(GCN_out, torch.tanh)
        self.ntn=NeuralTensorNetwork(GCN_out, tensor_neurons)
        self.actor_critic=actorCritic
        self.optimizer=optim.Adam(self.parameters(),lr=learning_rate)
        self.loss=nn.MSELoss()

    def forward(self,observation):
        sn_graph=dgl.batch([el.sn.graph for el in observation])
        vnr_graph=dgl.batch([el.vnr.graph for el in observation])
        h_sn=sn_graph.ndata['features']
        h_vnr=vnr_graph.ndata['features']
        h_sn=self.gcn_sn(sn_graph,h_sn)
        h_vnr=self.gcn_vnr(vnr_graph,h_vnr)
        sn_graph.ndata['h']=h_sn
        vnr_graph.ndata['h']=h_vnr
        
        sn_rep=[el.ndata['h'] for el in dgl.unbatch(sn_graph)]
        vnr_rep=[el.ndata['h'] for el in dgl.unbatch(vnr_graph)]
        
        state=[self.ntn(self.att_sn(sn_rep[i]),self.att_vnr(vnr_rep[i],observation[i].idx)) for i in range (len(observation))]
        state=torch.stack(state).view(len(observation),-1)
        value, policy_dist=self.actor_critic(state)
        return value, policy_dist
        

class GNNA2C1net(GNNA2C):
    def __init__(self, num_inputs_sn,num_inputs_vnr, hidden_size,GCN_out,tensor_neurons,num_actions,learning_rate):
        super().__init__(num_inputs_sn,num_inputs_vnr, hidden_size,GCN_out,tensor_neurons,num_actions,learning_rate,ActorCriticOneNetwork(tensor_neurons, num_actions, hidden_size))


class GNNA2C2net(GNNA2C):
    def __init__(self, num_inputs_sn,num_inputs_vnr, hidden_size,GCN_out,tensor_neurons,num_actions,learning_rate):
        super().__init__(num_inputs_sn,num_inputs_vnr, hidden_size,GCN_out,tensor_neurons,num_actions,learning_rate,ActorCritic(tensor_neurons, num_actions, hidden_size))