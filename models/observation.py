#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 10:17:42 2022

@author: kaouther
"""

class Observation():
    def __init__(self,sn,vnr,idx,node_mapping):
        self.sn=sn
        self.vnr=vnr
        self.idx=idx
        self.node_mapping=node_mapping