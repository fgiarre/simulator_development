#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 15:01:33 2022

@author: kaouther
"""

import torch 
import torch.nn as nn


class NeuralTensorNetwork(nn.Module):
    
    def __init__(self, embedding_dim, tensor_neurons):
        super(NeuralTensorNetwork, self).__init__()
        self.embedding_dim = embedding_dim
        self.tensor_neurons = tensor_neurons
        self.setup_weights()
        self.init_parameters()

    def setup_weights(self):
        self.weight_matrix = torch.nn.Parameter(
            torch.Tensor(self.embedding_dim, self.embedding_dim, self.tensor_neurons))
        self.weight_matrix_block = torch.nn.Parameter(torch.Tensor(self.tensor_neurons, 2 * self.embedding_dim))
        self.bias = torch.nn.Parameter(torch.Tensor(self.tensor_neurons, 1))
        
    def init_parameters(self):
        """
            Initializing weights.
        """
        torch.nn.init.xavier_uniform_(self.weight_matrix)
        torch.nn.init.xavier_uniform_(self.weight_matrix_block)
        torch.nn.init.xavier_uniform_(self.bias)
        
    def forward(self, embedding_1, embedding_2):
        
        scoring = torch.mm(torch.t(embedding_1), self.weight_matrix.view(self.embedding_dim, -1)).view(
            self.embedding_dim, self.tensor_neurons)
        scoring = torch.mm(torch.t(scoring), embedding_2)
        combined_representation = torch.cat((embedding_1, embedding_2))
        block_scoring = torch.mm(self.weight_matrix_block, combined_representation)
        scores = torch.tanh(scoring + block_scoring + self.bias)
        return scores


