#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 10:07:12 2022

@author: kaouther
"""
import networkx as nx
import numpy as np
from copy import deepcopy as dc
import dgl
import random
import matplotlib.pyplot as plt
import torch
import math
import torch.nn.functional as F
from scipy.special import softmax
import torch.nn as nn
from models.Agent import Agent
from models.observation import Observation
from DGLgraph import Graph as DGLgraph
import abc 

class Solver():
    
    def __init__(self,alpha):
        self.alpha=alpha
    
    def rev2cost(self,vnr):
        ve2seindex=vnr.edgemapping
        vsum_nodes = 0
        vsum_edeges = 0
        for i in range(len(vnr.vnode)):
            vsum_nodes = vsum_nodes + vnr.vnode[i].cpu
        for i in range(len(vnr.vedege)):
            vsum_edeges = vsum_edeges+ self.alpha * vnr.vedege[i].bandwidth
        dup=0
        for i in range(len(vnr.vedege)):
            if len(vnr.vedege[i].spc)>1:
                dup=dup+self.alpha *vnr.vedege[i].bandwidth*(len(vnr.vedege[i].spc)-1)
        vsum=vsum_nodes+vsum_edeges
        return vsum/(vsum+dup)
    
    def shortpath(self,G,fromnode,tonode,weight=None):
        try:
            sedegeindex=[]
            path=nx.dijkstra_path(G,fromnode,tonode,weight=weight)
            for i in range(len(path)-1):
                sedegeindex.append(G[path[i]][path[i+1]]["index"])
            cost=nx.dijkstra_path_length(G,fromnode,tonode,weight=weight)
        except nx.NetworkXNoPath:
            return [],[]
        return sedegeindex,cost
    
    def Sn2_networkxG(self,snode,sedege,bandlimit=0):
        g=nx.Graph()
        for s in snode:
            g.add_node(s.index)
        en=len(sedege)
        for i in range(en):
            if  sedege[i].lastbandwidth>bandlimit:
                g.add_edge(sedege[i].nodeindex[0], sedege[i].nodeindex[1],weight=sedege[i].lastbandwidth,index=sedege[i].index)
        return g
    
    @abc.abstractmethod 
    def mapping(self,sb,vnr):
        return 
    
    @abc.abstractmethod 
    def nodemapping(self,sb,vnr):
        return 
    
    @abc.abstractmethod
    def scaling_down(self,vnr,sn,scaling_chaine):
        return

    @abc.abstractmethod
    def scaling_up(self,vnr,sn,scaling_chaine):
        return
    
    def edegemapping(self,asnode,asedege,vnr,v2sindex):
        success=True
        ve2seindex=[]
        ven=len(vnr.vedege)
        for i in range(ven):
            fromnode=vnr.vedege[i].nodeindex[0]
            tonode = vnr.vedege[i].nodeindex[1]
            fromnode=v2sindex[fromnode]
            tonode=v2sindex[tonode]
            bandlimit=vnr.vedege[i].bandwidth
            g=self.Sn2_networkxG(asnode,asedege,bandlimit)
            pathindex,cost=self.shortpath(g,fromnode,tonode)
            if not pathindex:
                return False,[]
            ve2seindex.append(pathindex)
        return success,ve2seindex
    

class GNNDRL(Solver):
    
    def __init__(self,alpha,sigma,gamma, learning_rate, epsilon, memory_size, batch_size, num_inputs_sn, num_inputs_vnr, hidden_size, GCN_out, tensor_neurons, num_actions):
        super().__init__(alpha)
        self.saved_reward=None
        self.saved_observation=None
        self.saved_action=None
        self.saved_done=None
        self.sigma=sigma
        self.agent=Agent(gamma, learning_rate, epsilon, memory_size, batch_size, num_inputs_sn, num_inputs_vnr, hidden_size, GCN_out, tensor_neurons, num_actions)
     
    def getReward(self,vnr,sn):
        r2c=self.rev2cost(vnr)
        p_load=0
        for i in range(vnr.num_vnfs):
            p_load=p_load+sn.snode[vnr.nodemapping[i]].p_load
        p_load=p_load/vnr.num_vnfs
        return r2c,p_load,self.sigma*r2c+(1-self.sigma)/math.exp(p_load)
    
    def mapping(self, sn, vnr):
        success=True
        num_vnfs=vnr.num_vnfs
        v2sindex=[] # mapping between nodes and substrate
        ve2seindex=[-1]*vnr.num_vedges 
        vnr.nodemapping=[-1]*num_vnfs
        sn_c=dc(sn)
        sn_graph=DGLgraph(sn_c)
        vnr_graph=DGLgraph(vnr)
        obs=Observation(sn_graph, vnr_graph, 0, [])
        if vnr.id>1:
            self.agent.store_transition(self.saved_observation, self.saved_action, self.saved_reward,obs, self.saved_done)
            self.agent.learn()
        for idx in range(num_vnfs):
            nsuccess,action,v2sindex=self.nodemapping(obs,sn_c,vnr,idx)
            if nsuccess:
                if idx>0:
                    nodes_mapped=[i for i in range(idx+1)]
                    vsuccess=self.edegemapping(sn_c, vnr,idx,nodes_mapped,ve2seindex) 
                    if vsuccess:
                        if idx == num_vnfs-1:
                            r2c,p_load,self.saved_reward=self.getReward(vnr, sn_c)
                            self.saved_observation=obs
                            self.saved_action=action
                            self.saved_done=True
                            vnr.edgemapping=ve2seindex
                            return success, vnr.nodemapping, vnr.edgemapping,r2c,p_load,self.saved_reward,sn_c
                        else:
                            reward=0
                            sn_graph=DGLgraph(sn_c)
                            vnr_graph=DGLgraph(vnr)
                            new_obs=Observation(sn_graph, vnr_graph, idx+1,v2sindex)
                            self.agent.store_transition(obs, action, reward, new_obs, False)
                            self.agent.learn()
                            obs=new_obs
                    else:
                        self.saved_reward=-1 
                        self.saved_observation=obs
                        self.saved_action=action
                        self.saved_done=True 
                        success=False
                        return success, [], [],0,0,self.saved_reward,sn
                else:
                    reward=0
                    sn_graph=DGLgraph(sn_c)
                    vnr_graph=DGLgraph(vnr)
                    new_obs=Observation(sn_graph, vnr_graph, idx+1,v2sindex)
                    self.agent.store_transition(obs, action, reward, new_obs, False)
                    self.agent.learn()
                    obs=new_obs
            else:
                self.saved_reward=-1 
                self.saved_observation=obs
                self.saved_action=action
                self.saved_done=True
                success=False
                return success, [], [],0,0,self.saved_reward,sn
           
    def nodemapping(self,observation,sn,vnr,idx):
        nsuccess=True
        action=self.agent.choose_action(observation)
        if sn.snode[action].lastcpu> vnr.vnode[idx].cpu:
            sn.snode[action].lastcpu-=vnr.vnode[idx].cpu
            sn.snode[action].vnodeindexs.append([vnr.id,idx])
            sn.snode[action].p_load=(sn.snode[action].p_load*sn.snode[action].cpu+vnr.vnode[idx].p_maxCpu)/sn.snode[action].cpu
            vnr.nodemapping[idx]=action
            vnr.vnode[idx].sn_host=action
        else:
            nsuccess=False
        return nsuccess,action,vnr.nodemapping
         
    def edegemapping(self,sn,vnr,idx,nodes_mapped,ve2seindex):
        success=True
        neighbors=vnr.vnode[idx].neighbors
        mapped=nodes_mapped
        intersection= np.intersect1d(neighbors,mapped)
        for i in intersection:
            if idx< i :
                s=idx
                d=i
            else: 
                s=i
                d=idx
            edege_list=list(vnr.graph.edges())
            index=edege_list.index((s,d))
            bw=vnr.vedege[index].bandwidth
            fromnode=vnr.nodemapping[s]
            tonode=vnr.nodemapping[d]
            g=self.Sn2_networkxG(sn.snode,sn.sedege,bw)
            pathindex,cost=self.shortpath(g,fromnode,tonode)
            if not pathindex:
                return False
            for j in pathindex:
                sn.sedege[j].lastbandwidth-=vnr.vedege[index].bandwidth
                sn.sedege[j].vedegeindexs.append([vnr.id,index])
                nodeindex=sn.sedege[j].nodeindex
                sn.snode[nodeindex[0]].lastbw-=vnr.vedege[index].bandwidth
                sn.snode[nodeindex[1]].lastbw-=vnr.vedege[index].bandwidth
            vnr.vedege[index].spc = pathindex
            ve2seindex[index]=pathindex
        return success
    
    def scaling_down(self,vnr,sn,scaling_chaine):
        for i in scaling_chaine:
            vnr.vnode[i].cpu-=vnr.vnode[i].req_cpu
            sn.snode[vnr.vnode[i].sn_host].lastcpu+=vnr.vnode[i].req_cpu
        return sn
    
    def find_remapping_edeges(self,vnr,remapping_nodes):
        remapping_edeges=[]
        for i in range(len(vnr.vedege)):
            nodeindex=np.array(vnr.vedege[i].nodeindex)
            nodes=nodeindex[~np.isin(nodeindex,remapping_nodes)]
            
            if len(nodes)<2:
                remapping_edeges.append(i)  
        return remapping_edeges
    def remove_edeges_mapping(self,vnr,sn_c,remapping_edeges):
        #remove edges mapping
        edges=[[vnr.id, i] for i in remapping_edeges]
        for ed in edges:
            for i in range(len(sn_c.sedege)):
                sed=sn_c.sedege[i].vedegeindexs
                if ed in sed:
                    sn_c.sedege[i].vedegeindexs.remove(ed)
                    nodeindex=sn_c.sedege[i].nodeindex
                    sn_c.snode[nodeindex[0]].lastbw+=vnr.vedege[ed[1]].bandwidth
                    sn_c.snode[nodeindex[1]].lastbw+=vnr.vedege[ed[1]].bandwidth
                    sn_c.sedege[i].lastbandwidth+=vnr.vedege[ed[1]].bandwidth
            vnr.vedege[ed[1]].spc=[]
        return sn_c
            
    
    def remove_nodes_mapping(self,vnr,sn_c,remapping_nodes):
        for idx in remapping_nodes:
            sn_c.snode[vnr.vnode[idx].sn_host].lastcpu+=vnr.vnode[idx].cpu
            sn_c.snode[vnr.vnode[idx].sn_host].vnodeindexs.remove([vnr.id,idx])
            sn_c.snode[vnr.vnode[idx].sn_host].p_load=(sn_c.snode[vnr.vnode[idx].sn_host].p_load*sn_c.snode[vnr.vnode[idx].sn_host].cpu-vnr.vnode[idx].p_maxCpu)/sn_c.snode[vnr.vnode[idx].sn_host].cpu
            vnr.vnode[idx].sn_host=-1
            vnr.nodemapping[idx]=-1
        return sn_c
                
    def remapping_sub_vnr(self,vnr,sn,remapping_nodes,remapping_edeges):
        success=True
        num_vnfs=len(remapping_nodes)
        v2sindex=[] # mapping between nodes and substrate
        ve2seindex=[-1]*vnr.num_vedges 
        sn_c=dc(sn)
        #remove edges mapping
        self.remove_edeges_mapping(vnr, sn_c, remapping_edeges)
        #remove nodes mapping
        self.remove_nodes_mapping(vnr, sn_c, remapping_nodes)
        nodemapping=vnr.nodemapping
        ve2seindex=vnr.edgemapping
        #remapping 
        sn_graph=DGLgraph(sn_c)
        vnr_graph=DGLgraph(vnr)
        obs=Observation(sn_graph, vnr_graph,remapping_nodes[0],nodemapping)
        #store the last transition
        self.agent.store_transition(self.saved_observation, self.saved_action, self.saved_reward,obs, self.saved_done)
        self.agent.learn()
        for i in range(len(remapping_nodes)):
            idx=remapping_nodes[i]
            vnr.vnode[idx].cpu+=vnr.vnode[idx].req_cpu
            nsuccess,action,v2sindex=self.nodemapping(obs,sn_c,vnr,idx)
            if nsuccess:
                nodes_mapped=[i for i in range(len(vnr.nodemapping)) if vnr.nodemapping[i]>-1  ]
                vsuccess=self.edegemapping(sn_c, vnr,idx,nodes_mapped,ve2seindex) 
                if vsuccess:
                    if i== len(remapping_nodes)-1:
                        r2c,p_load,self.saved_reward=self.getReward(vnr, sn_c)
                        self.saved_observation=obs
                        self.saved_action=action
                        self.saved_done=True
                        vnr.edgemapping=ve2seindex
                        return success, vnr.nodemapping, vnr.edgemapping,r2c,p_load,self.saved_reward,sn_c   
                    else:
                         reward=0
                         sn_graph=DGLgraph(sn_c)
                         vnr_graph=DGLgraph(vnr)
                         new_obs=Observation(sn_graph, vnr_graph, remapping_nodes[i+1],v2sindex)
                         self.agent.store_transition(obs, action, reward, new_obs, False)
                         self.agent.learn()
                         obs=new_obs
            if not nsuccess:
               vnr.vnode[idx].cpu-=vnr.vnode[idx].req_cpu
            if not nsuccess or not vsuccess:
                    self.saved_reward=-1 
                    self.saved_observation=obs
                    self.saved_action=action
                    self.saved_done=True 
                    success=False
                    return success, [], [],0,0,self.saved_reward,sn_c
            
               
    def scaling_up(self,vnr,sn,scaling_chaine):
        sn_c=dc(sn)
        remapping_nodes=[]
        for i in scaling_chaine:
            if sn_c.snode[vnr.vnode[i].sn_host].lastcpu >vnr.vnode[i].req_cpu:
                #vertical scalability
                sn_c.snode[vnr.vnode[i].sn_host].lastcpu-=vnr.vnode[i].req_cpu
                vnr.vnode[i].cpu+=vnr.vnode[i].req_cpu
            else:
                remapping_nodes.append(i)
        remap=False        
        if len(remapping_nodes)>0:
            remap=True
            remapping_edeges=self.find_remapping_edeges(vnr,remapping_nodes)
            success,nodemapping,edgemapping,r2c,p_load,reward,sn_c=self.remapping_sub_vnr(vnr, sn_c, remapping_nodes, remapping_edeges)        
            return success,nodemapping,edgemapping,r2c,p_load,reward,remap,sn_c
        else:
            return True,vnr.nodemapping,vnr.edgemapping,0,0,1,remap,sn_c
    
class FirstFit(Solver):
    
    def __init__(self,alpha):
        super().__init__(alpha)
        
    def nodemapping(self, sb, vnr):
        success=True # turn to false if failure of placement
        vn=vnr.num_vnfs # number of nodes to place
        v2sindex=[] # mapping between nodes and substrate
        # choose a random order of substrate nodes to select from
        sn = random.sample(list(range(len(sb.snode))), len(sb.snode))
        for vi in range(vn):
            for i,si in enumerate(sn):
                if vnr.vnode[vi].cpu<sb.snode[si].lastcpu and si not in  v2sindex:
                    v2sindex.append(si) 
                    break
                else:
                    if (i==len(sn)-1) : # no substrate node available for placement
                        success=False
                        return success,[]
        return success, v2sindex
    
    def mapping(self,sb,vnr):
        # parameters needed:
        success=True # turn to false if failure of placement
        vn=vnr.num_vnfs # number of nodes to place
        v2sindex=[] # mapping between nodes and substrate
        vese2index=[] # mapping between edeges and substrate
        nodesuccess, v2sindex=self.nodemapping(sb, vnr)
        if nodesuccess:
            edgesuccess, vese2index=self.edegemapping(sb.snode, sb.sedege, vnr, v2sindex)
        if nodesuccess and edgesuccess:
            for i in range(vn):
                sb.snode[v2sindex[i]].lastcpu=sb.snode[v2sindex[i]].lastcpu-vnr.vnode[i].cpu
                sb.snode[v2sindex[i]].vnodeindexs.append([vnr.id,vnr.vnode[i].index])
                vnr.vnode[i].sn_host = v2sindex[i]
            for i in range(len(vese2index)):
                pathindex=vese2index[i]
                for j in pathindex:
                    sb.sedege[j].lastbandwidth=sb.sedege[j].lastbandwidth-vnr.vedege[i].bandwidth
                    sb.sedege[j].vedegeindexs.append([vnr.id,i])
                vnr.vedege[i].spc = pathindex
            vnr.nodemapping=v2sindex
            vnr.edgemapping=vese2index
            r2c=self.rev2cost(vnr)
            return success,v2sindex,vese2index,r2c
        else:
            return False,[],[],-1


        

    
 