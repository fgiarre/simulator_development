#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 15:01:33 2022

@author: kaouther
"""

import torch 
import torch.nn as nn
import dgl
import dgl.function as fn


# We first define the message and reduce function
gcn_msg = fn.copy_src(src='h', out='m')
gcn_reduce = fn.sum(msg='m', out='h')


class ApplyNodes(nn.Module):
    
    def __init__(self, in_feats, out_feats, activation):
        super(ApplyNodes, self).__init__()
        self.linear = nn.Linear(in_feats, out_feats)
        self.activation = activation
    
    def forward(self, node):
        h = self.linear(node.data['h'])
        h = self.activation(h)
        return {'h' : h} 


class GCNLayer(nn.Module):
    
    def __init__(self, in_feats, out_feats,activation):
        super(GCNLayer, self).__init__()
        self.linear = ApplyNodes(in_feats, out_feats, activation)
    def forward(self, g, feature):
        g.ndata['h'] = feature
        g.update_all(gcn_msg, gcn_reduce)
        g.apply_nodes(func=self.linear)
        return g.ndata.pop('h')
        
class GCNModule(nn.Module):
      
   def __init__(self,in_feats,hidden_dim,out_feats,activation):
        super(GCNModule, self).__init__()
        self.layer1 = GCNLayer(in_feats, hidden_dim, activation)
        self.layer2 = GCNLayer(hidden_dim, hidden_dim, activation)
        self.layer3 = GCNLayer(hidden_dim, out_feats, activation)

   def forward(self, g, features):
       h=self.layer1(g,features)
       h=self.layer2(g,h)
       h=self.layer3(g,h)
       return h
              
 

       



