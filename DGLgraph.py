#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 22 14:29:35 2022

@author: kaouther
"""
import dgl 
import torch

class Graph():
    
    def __init__(self,network): 
        self.network=network
        self.graph=self.generatGraph(network.getNetworkx())
    
    def getFeatures(self):
        features=self.graph.ndata['features'] 
        return features
    
    def getGraph(self):
        return self.graph
    
    def generatGraph(self,networkx):
        graph=dgl.from_networkx(networkx)
        features=self.network.getFeatures()
        graph.ndata['features'] =torch.tensor(features).type('torch.FloatTensor')  
        return graph
    
    def updateFeatures(self,features):
        self.graph.ndata['features'] = torch.tensor(features).type('torch.FloatTensor')
        

