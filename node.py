#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 14:41:52 2022

@author: kaouther
""" 

import abc



class Node:
    
    def __init__(self,index,cpu):
        self.index=index
        self.cpu=cpu
        self.bw = 0
        self.neighbors = []
        self.degree = 0
        
    @abc.abstractmethod   
    def __str__(self):
        ''' This method returns a string of node characteristics to be printed in msg'''
        return 
    
    def msg(self):
        print(self.__str__())

class Vnf(Node):
    
    def __init__(self, index,cpu,cpu_max,req,flavor_size,p_scalingUp):
        super().__init__(index, cpu)
        self.flavor=[]
        self.cpu_index=0
        i=1
        while (i <= flavor_size and i*self.cpu <= cpu_max):
            self.flavor.append(i*self.cpu)
            i+=1
        if p_scalingUp*self.cpu>self.flavor[len(self.flavor)-1]:
            self.p_maxCpu=self.flavor[len(self.flavor)-1]
        else:
            self.p_maxCpu=p_scalingUp*self.cpu
        self.req = req
        self.sn_host = None
        self.req_cpu=0
        
    def __str__(self):
       return 'vnf',self.index,'cpu',self.cpu,'cpu index',self.cpu_index,'flavor',self.flavor,'max_cpu',self.p_maxCpu,'bw',self.bw,'neighbors',self.neighbors,'placement',self.sn_host 
   
    def dm_scaling(self,scale_up):
        # scale up/ down if flavor allow 
        self.req_cpu=self.cpu
        if scale_up==True and self.cpu_index<len(self.flavor)-1:
            self.req_cpu=self.flavor[self.cpu_index+1]
            self.cpu_index+=1
        elif scale_up==False and self.cpu_index>0:
            self.req_cpu= self.flavor[self.cpu_index-1]
            self.cpu_index-=1
        return self.req_cpu-self.cpu, self.req_cpu
    

        
class Snode(Node):
    
    def __init__(self,index,cpu):
        super().__init__(index, cpu)
        self.lastcpu=cpu
        self.lastbw =0
        self.vnodeindexs = []
        self.p_load=0
        

    def __str__(self):
        return 'snode', self.index, 'cpu', self.cpu, 'lastcpu', self.lastcpu,'vnodeindexs',self.vnodeindexs,'neighbors', self.neighbors,'bw',self.bw,'lastbw',self.lastbw,'p_load',self.p_load

