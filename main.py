from generator import *
from mano import ManoSimulator
from solver import *
from substrate import SN
import matplotlib.pyplot as plt
from termcolor import colored
import simpy
import pickle
import networkx as nx
import copy
import json
import math
import os
import time
# to measure exec time 
from timeit import default_timer as timer 

# Opening JSON file 
with open('parameters.json', 'r') as openfile: 
    # Reading from json file 
    json_object = json.load(openfile) 
    
OUTPUT_PATH=json_object['OUTPUT_PATH']
SIM_TIME=json_object['SIM_TIME']
REPEAT_EXPERIENCE=json_object['REPEAT_EXPERIENCE']
Controller=json_object['Controller']
MST=json_object['MST']

# Substrate related parameters
alpha = json_object['alpha']
beta=json_object['beta']
cpu_range = json_object['cpu_range']
numnodes = json_object['numnodes']
bw_range = json_object['bw_range']
# Virtual network related parameters
num_reqs = json_object['num_reqs'] # number of simultaneous requests for the vnr generator
vnfs_range =json_object['vnfs_range']
vcpu_range =json_object['vcpu_range']
vbw_range =json_object['vbw_range']
start_timesteps=json_object['start_timesteps']
start_mean_calculation=json_object['start_mean_calculation']
N_iter=json_object['N_iter']
MTBA = json_object['MTBA']     # Mean Time Between Arrival
MLT=json_object['MLT']         # Mean life time of each cass of VNR
MTBS= json_object['MTBS']      # Mean time between scale demande of each class of VNR
vnr_classes=json_object['vnr_classes'] #Proportion of vnr classes 
p_flavors=json_object['p_flavors']
flavor_tab=json_object['flavors']

# For TD3 #
expl_noise=json_object['expl_noise']
policy_noise=json_object['policy_noise']
policy_clip=json_object['noise_clip']

np.random.seed(seed=100)
Seeds = [317805,7671309,222111,310320]#np.random.randint(42947296, size=REPEAT_EXPERIENCE)



def plottingAxisOne(arrx1,arry1, strategy='strategy',filename = 'filename', xlabel='timesteps [s]',ylabel = 'R2C', legend = True):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.style.use('fivethirtyeight')
    
    plt.plot(arrx1, arry1, linewidth=0.5, label=strategy)
    if legend:
        plt.legend(loc='upper right')
    plt.tight_layout()
    ax = plt.savefig(OUTPUT_PATH+filename+".pdf", format='pdf')
    pickle.dump(ax, open(OUTPUT_PATH+filename+".pickle", "wb"))
    plt.close()
    return

def mean_res_value(Res,i):
    Res=np.array(Res, dtype="object")
    Res=Res[:,i]
    return np.mean(Res[math.floor(6*len(Res)/7):len(Res)])

Results=[]
R2C=[0]*len(MTBA)
VNF_deployed=[0]*len(MTBA)
vegdes_deployed=[0]*len(MTBA)
VNR_deployed=[0]*len(MTBA)
MSE=[]


# Create a substrate environment
topology  = 'NetworkAll_BtEurope.matrix'
topology =  nx.Graph(np.loadtxt(topology, dtype=int))
old_subNet= SN(numnodes, cpu_range, bw_range,topology)
old_subNet.drawSN()

with open('topology.pkl', 'wb') as output:
    pickle.dump(old_subNet, output, pickle.HIGHEST_PROTOCOL)
    
print(colored('Experience started', 'green'))
for i in range(len(MTBA)):
    print('%d' % i)
    np.random.seed(seed=np.random.randint(Seeds[i], size=REPEAT_EXPERIENCE)) #Seeds[i]
    
    env = simpy.Environment()
    subNet = copy.deepcopy(old_subNet)
    print('Strategy selected:',Controller)
    
    if Controller=="firstfit":
        solver=FirstFit(alpha)
    if Controller=="GNNDRL":
        solver=GNNDRL(alpha, 0.85,0.98, 5e-4, 1, 1000000, 64, 3, 3, 128, 128, 8, 24)
    manoSimulator=ManoSimulator(solver,subNet,env)
    start=time.time()
    generator=Generator(vnr_classes, MLT, MTBS, MTBA[i], vnfs_range, vcpu_range, vbw_range, flavor_tab, p_flavors)  
    env.process(generator.VnrGenerator_poisson(env,subNet,manoSimulator))
    # Execute!
    env.run(until=SIM_TIME[i])
    print(time.time()-start)
    Results.append(manoSimulator.obtained_results)
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][0] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test1', xlabel='timesteps [s]',ylabel = 'aR2C+(1-a)p_load')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][1] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test2', xlabel='timesteps [s]',ylabel = 'R2C')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][2] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test3', xlabel='timesteps [s]',ylabel = 'p_charge')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][3] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test4', xlabel='timesteps [s]',ylabel = 'taux_refus')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][4] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test5', xlabel='timesteps [s]',ylabel = 'taux_replacement')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][5] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test6', xlabel='timesteps [s]',ylabel = 'charge')
    plottingAxisOne([i+1 for i in range(len(manoSimulator.episodes_results))], [manoSimulator.episodes_results[i][6] for i in range(len(manoSimulator.episodes_results))],strategy='GNNDQN',filename = 'test7', xlabel='timesteps [s]',ylabel = 'nb_noued')
    print(manoSimulator.episodes_results)
    # calculate the mean R2C
    R2C[i]=mean_res_value(Results[i],1)
    VNF_deployed[i]=mean_res_value(Results[i],3)
    vegdes_deployed[i]=mean_res_value(Results[i],4)
    VNR_deployed[i]=mean_res_value(Results[i],5)
    
    rfile = 'Results/'+Controller+'_Rewards.txt'
    f = open(rfile,"a+")
    print("MTBA:", MTBA[i])
    print("R2C=",R2C)
    print("VNF_deployed=",VNF_deployed)
    print("vegdes_deployed=",vegdes_deployed)
    print("VNR_deployed=",VNR_deployed)
    
    f.write("MTBA: %f\n" % (MTBA[i]))
    f.write("R2C: %f:\n" % (R2C[i]))
    f.write("R2C: %f:\n" % (VNF_deployed[i]))
    f.write("R2C: %f:\n" % (vegdes_deployed[i]))
    f.write("R2C: %f:\n" % (VNR_deployed[i]))
    f.close()
    
    
    # save the results
    with open("results_"+Controller+".pkl", 'wb') as output:
        pickle.dump([Results,R2C], output, pickle.HIGHEST_PROTOCOL)
    
    del env
    del subNet