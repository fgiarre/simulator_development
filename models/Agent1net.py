#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 10:55:48 2022

@author: kaouther
"""
from models.A2C import GNNA2C1net as GNNA2C
import numpy as np
import torch
from models.replayBuffer import ReplayBuffer

class Agent(object):
    
    def __init__(self,gamma,learning_rate,epsilon,memory_size,batch_size,num_inputs_sn, num_inputs_vnr, hidden_size, GCN_out, tensor_neurons, num_actions,eps_min = 0.01, eps_dec = 5e-7,replace = 1000):
        self.gamma=gamma
        self.epsilon=epsilon
        self.num_actions=num_actions
        self.memory=ReplayBuffer(memory_size)
        self.batch_size = batch_size
        self.eps_min = eps_min
        self.eps_dec = eps_dec
        self.replace_target_cntr = replace
        self.learn_step_counter = 0
        self.q_eval=GNNA2C(num_inputs_sn, num_inputs_vnr, hidden_size, GCN_out, tensor_neurons, num_actions, learning_rate)
        self.q_next=GNNA2C(num_inputs_sn, num_inputs_vnr, hidden_size, GCN_out, tensor_neurons, num_actions, learning_rate)
        
    def store_transition(self,state,action,reward,state_,done):
        self.memory.store(state,action,reward,state_,done)
        
    def choose_action(self,observation):
        illegal_actions=observation.node_mapping
        legal_actions=np.array(range(self.num_actions))
        legal_actions=legal_actions[~np.isin(legal_actions,illegal_actions)]
        if np.random.random() > self.epsilon:
            _,policy_dist= self.q_eval([observation])
            policy_dist[0][illegal_actions] = -float('Inf')
            action = torch.argmax(policy_dist[0]).item()
        else:
            action = np.random.choice(legal_actions)
        return action
    
    def replace_target_network(self):
        if self.replace_target_cntr is not None and self.learn_step_counter % self.replace_target_cntr == 0:
            self.q_next.load_state_dict(self.q_eval.state_dict())

    def decrement_epsilon(self):
        self.epsilon = self.epsilon - self.eps_dec if self.epsilon > self.eps_min else self.eps_min

    def learn(self):
        if self.memory.mem_cntr < self.batch_size:
            return
        self.q_eval.optimizer.zero_grad()
        self.replace_target_network()
        states, actions, rewards, new_states, dones = self.memory.sample_buffer(self.batch_size)
        actions = torch.tensor(actions)
        rewards = torch.tensor(rewards)
        V_s,A_s = self.q_eval(states)
        V_s_,A_s_ = self.q_next(new_states)
        q_pred = torch.add(V_s, (A_s - A_s.mean(dim=1, keepdim=True))).gather(1, actions.unsqueeze(-1)).squeeze(-1).type('torch.DoubleTensor')
        q_next = torch.add(V_s_, (A_s_ - A_s_.mean(dim=1, keepdim=True)))
        q_target = rewards + (self.gamma * torch.max(q_next, dim=1)[0].detach()).type('torch.DoubleTensor')
        loss = self.q_eval.loss(q_target, q_pred)
        loss.backward()
        self.q_eval.optimizer.step()
        self.learn_step_counter += 1
        self.decrement_epsilon()


    